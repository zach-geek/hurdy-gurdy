require("DAC")
require("math")
require("GPIO")

g <- GPIO(4);
g.input();
g.pullup(true);

// Create an instance for DAC0
dac <- DAC(0);

// Set AOUT to output 1.65V
dac.writev(1.65);

// Set the output frequency to 100kHz
dac.frequency(50000);

i <- 0.0;

pitches <- [262, 330, 392, 494]
cpitch <- 0

function nextPitch()
{
    cpitch = (cpitch + 1) % pitches.len()
}

//g.onfalling(nextPitch);

b1 <- blob()
b2 <- blob()
b3 <- blob()

bs <- [b1 b2 b3]

cb <- 0

pitchWaves <- []

function setPitches()
{
    pitchWaves.clear();

    for (local cp = 0; cp < pitches.len(); cp++)
    {
        local sinewave = blob()
        local f = pitches[cp]
        for (local i = 0; i < f - 1; i++)
      		//sinewave.writen(sin(i*2.0*PI/f)*2047+2047, 'w');
            sinewave.writen(((sin(i*2.0*PI/f) > 0) ? 1023 : 0) + sin(i*2.0*PI/f)*1023+1023, 'w');
        pitchWaves.append(sinewave)
    }
}

setPitches()


while (true) {
    if (g.islow())
        nextPitch()
   local sinewave = pitchWaves[cpitch];

  local b = bs[cb]
  b.seek(0)
  while (b.tell() < 8200)
       b.writeblob(sinewave)
  b.resize(b.tell())
  dac.waitready()
  dac.writeblob(b)
  cb = (cb + 1) % 3
  delay(1)
}
