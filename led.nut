pwm <- PWM(0)
pwm.frequency(2000);
lastColor <- [0, 0, 0]
function setColor(p)
{
  print("color:")
  foreach (c in p)
  {
    print(" " + c)
  }
  print("\n")

  pwm.on(0);
  pwm.on(1);
  pwm.on(2);

  local allSet = false;

  while (!allSet)
  {
    allSet = true;
    for (local i = 0; i < 3; i++)
    {
      if (lastColor[i] < p[i])
      {
        allSet = false;
        lastColor[i]++;
        pwm.duty_cycle(i, 100 - lastColor[i]);
      }

      if (lastColor[i] > p[i])
      {
        allSet = false;
        lastColor[i]--;
        pwm.duty_cycle(i, 100 - lastColor[i]);
      }
    }

    delay(10);
  }
}

setColor(lastColor)
