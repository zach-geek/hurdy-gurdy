$("#note-on").click ->
  console.log $("#note")[0].value
  erpc("noteOn", $("#note")[0].value)
$("#note-off").click ->
  erpc("noteOff", $("#note")[0].value)

addButton = (button, note) ->
  bbox = $("<div class='buttonbox'><div class='label flex'>#{button}</div></div>")
  valbox = $("<input type='text' value='#{note.note}' class='note'>")
  shiftbox = $("<input type='text' value='#{note.shifted}' class='note'>")
  lowbox = $("<input type='text' value='#{note.lowered}' class='note'>")
  bbox.append(valbox)
  bbox.append(shiftbox)
  bbox.append(lowbox)
  $("#notes").append(bbox)

addDrone = (drone) ->
  container = $("<div class='buttonbox'><div class='label'>#{drone}</div></div>")
  cb = $("<input type='checkbox'>")
  cb.click ->
    if cb.is(":checked")
      erpc("noteOn", drone)
    else
      erpc("noteOff", drone)
  container.append(cb)
  $("#notes").append(container)

getTunings = ->
  erpc "getTunings", null, (tunings) ->
    window.$tunings = tunings
    current_tuning = null
    $("#tuning").empty()
    for name, keys of tunings
      console.log("Option: #{name}")
      setupTuning(name) if keys.current_tuning
      $("#tuning").append("<option#{if current_tuning then " selected" else ""}>#{name}</option>")


setupTuning = (tuning)->
  console.log("New tuning #{tuning}")
  $("#notes").empty()
  b = 0
  addButton(b++, n) for n in $tunings[tuning].buttons
  addDrone(d) for d in $tunings[tuning].drones


$("#tuning").change ->
  tuning = $("#tuning")[0].value
  setupTuning(tuning)
  erpc("setTuning", tuning)

getTunings()
