dofile("sd:/hurdygurdy/synth.nut")
dofile("sd:/hurdygurdy/buttons.nut")

bb <- Button(8, true)

s <- SynthSink()

notes <- ["C4", "G3", "A4", "D4"]
cpitch <- 0

function nextPitch()
{
    cpitch = (cpitch + 1) % pitches.len()
}

s.droneOn("C4")

while (true) {
  if (bb.pressed())
  {
    nextNote();
    s.noteOn(notes[cpitch])
  }

  delay(100)
}
