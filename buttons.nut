class Button
{
  constructor(_pin, _activeLow) {
    activeLow = _activeLow
    pin = _pin
    gpio = GPIO(pin)
    gpio.input()
    gpio.pullup(true)
  }

  function pressed() {
    return gpio.islow() == activeLow
  }

  activeLow = true;
  gpio = null;
  pin = null;
}

class NoteButton extends Button
{
  constructor(_pin, _note, _shifted, _activeLow = false)
  {
    base.constructor(_pin, _activeLow)
    note = _note
    shifted = _shifted
  }

  note = null
  shifted = null
  lowered = null
  special = null
}

class OpenNoteButton extends NoteButton
{
  constructor(_note, _shifted)
  {
    base.constructor(0, _note, _shifted, true)
    gpio.output()
    gpio.low()
  }
}

shifter <- Button(8, true)
downShifer <- Button(13, true)

buttons <- [
  OpenNoteButton("G4", "E5"),
  NoteButton(9, "A4", "F5", true),
  NoteButton(10, "B4", "G5", true),
  NoteButton(11, "C5", "A5", true),
  NoteButton(12, "D5", "C6", true)
]

drones <- [
  Button(25, false)
  Button(27, false)
  Button(26, false)
]
