require("Socket");

class MidiSink {
  socket = null
  readCount = null
  lastPressure = null
  constructor() {
    socket = Socket();
    readCount = 0
    lastPressure = -1
    connect();
  }

  function connect() {
    try {
      print("connecting...\n")
      delay(1)
      print("but really now\n")
      socket.connect("voyager.home", 2023)
    } catch (e)
    {
      print("Error: " + e  + "\n")
      delay(1)
    }
  }

  function doMidi(cmd) {
    socket.writestr(cmd + "\n");
    readCount += 1
  }

  function waitForProc() {
    while (readCount > 0)
    {
    while (socket.read() == null)
    {
      delay(10)
    }
    readCount -= 1
    }
  }

  function doNote(action, noteStr) {
    local octave = noteStr.slice(0,1)
    local note = noteStr.slice(1,2)

    if (lastOctave != octave)
    {
      doMidi("octave " + octave)
      lastOctave = octave
    }
    doMidi(action + " \"" + note + "\"")
  }

  function noteOn(noteStr) {
    doNote("note", noteStr)
  }

  function noteOff(noteStr) {
    doNote("note_off", noteStr)
  }

  function setPressure(pressure) {
    local p = floor(pressure * 127)

    if (p != lastPressure) {
      /*sendCommand({command = "note_pressure", pressure = p})*/
      //doMidi("velocity " + p)
      doMidi("control_change 7, " + p)
      //doMidi("channel_aftertouch " + p)
      lastPressure = p;
    }
  }

  function endLoop() {
    waitForProc();
    delay(1);
  }
}
