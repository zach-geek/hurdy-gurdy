require("JSON")
require("GPIO")

dofile("sd:/hurdygurdy/led.nut")

tunings <- {}
tuningIndex <- 0
tuningKeys <- []
droneNotes <- []

function getTunings() {
  print("Getting tunings...\n")
  return tunings;
}

function loadTunings() {
  local f = file("sd:/hurdygurdy/tunings.json", "r")
  tunings = jsondecode(f.readstr(1024 * 5));
  f.close();

  tuningKeys.clear()
  foreach (k, t in tunings) {
    tuningKeys.append(k)
  }
}

function getShiftAmmount() {
  local direction = 0;
  if (shifter.pressed())
  {
    direction = 1;
  }
  else if (downShifer.pressed())
  {
    direction = -1;
  }
  else
  {
    return 0;
  }

  local ammount = 0
  local index = 0
  foreach (b in buttons) {
    index++
    if (b.pressed()) {
      ammount = index
    }
  }

  return ammount * direction;
}

AllNotes <- ["B0", "C1", "CS1", "D1", "DS1", "E1", "F1", "FS1", "G1", "GS1", "A1", "AS1", "B1", "C2", "CS2", "D2", "DS2", "E2", "F2", "FS2", "G2", "GS2", "A2", "AS2", "B2", "C3", "CS3", "D3", "DS3", "E3", "F3", "FS3", "G3", "GS3", "A3", "AS3", "B3", "C4", "CS4", "D4", "DS4", "E4", "F4", "FS4", "G4", "GS4", "A4", "AS4", "B4", "C5", "CS5", "D5", "DS5", "E5", "F5", "FS5", "G5", "GS5", "A5", "AS5", "B5", "C6", "CS6", "D6", "DS6", "E6", "F6", "FS6", "G6", "GS6", "A6", "AS6", "B6", "C7", "CS7", "D7", "DS7", "E7", "F7", "FS7", "G7", "GS7", "A7", "AS7", "B7", "C8", "CS8", "D8", "DS8"]

function shiftNote(note, ammount)
{
  if (ammount == 0)
  {
    return note;
  }

  local idx = AllNotes.find(note)

  if (idx == null)
  {
    print("No such note " + note)
    return note
  }

  return AllNotes[idx + ammount]
}

function setTuning(tuningName) {
  local shift = getShiftAmmount()
  print("Changing to " + tuningName + " tuning + " + shift + "\n")
  local tuning = tunings[tuningName]

  for (local i = 0; i < tuning.buttons.len(); i++) {
    buttons[i].note = shiftNote(tuning.buttons[i].note, shift);
    buttons[i].shifted = shiftNote(tuning.buttons[i].shifted, shift);
    buttons[i].lowered = shiftNote(tuning.buttons[i].lowered, shift);
    buttons[i].special = shiftNote(tuning.buttons[i].special, shift);
  }

  droneNotes.clear()
  foreach (d in tuning.drones) {
    droneNotes.append(shiftNote(d, shift))
  }

  setColor(tuning.color)
}

loadTunings();
setTuning(tuningKeys[tuningIndex])


class Knob {
  pinA = null
  pinB = null
  counter = null
  lastState = null
  proceed = null
  constructor(_pinA, _pinB) {
    pinA = GPIO(_pinA)
    pinB = GPIO(_pinB)
    pinA.input()
    pinA.pullup(true)
    pinB.input()
    pinB.pullup(true)

    pinB.onfalling(pinfall.bindenv(this))

    counter = 0
    lastState = 0
    proceed = false
  }

  function read() {
    if (proceed)
    {
      if (!pinB.islow())
      {
        proceed = false
        return true;
      }
    }
    return false;
  }

  function pinfall() {
    proceed = true
  }
}

class TuningKnob extends Knob {
  constructor(_pinA, _pinB) {
    base.constructor(_pinA, _pinB)
  }

  function checkTuning() {
    if (read()) {
      setTuning(tuningKeys[++tuningIndex % tuningKeys.len()])
    }
  }
}

tuningKnob <- TuningKnob(22, 23);
