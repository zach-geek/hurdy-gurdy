require(["ADC", "math","string"]);

adc <- ADC(0);
lastV <- 0.0;
function getCrank() {
  local rawv = adc.readv(0)
  const minV = 0.0032
  const maxV = 0.04
  /*const maxV = 0.05*/

  local v = ((rawv - minV) + (lastV - minV)) / (2.0 * maxV)
  lastV = rawv

  if (v > 0.0)
  {
    /*print("V: " + v + " LV: " + lastV + "\n")*/
    /*v = (lastV + (v / maxV)) / 2.0*/
    if (v > 1.0)
      v = 1.0
    /*lastV = v;*/
    return (exp(v) - 1.0) / (exp(1.0) - 1.0);
  }
  return 0;
}

function doCrankServer() {
  require("WebSocket");
  server <- WebSocketServer();

  print("waiting for connection\n");
  while (!server.available())
  delay(10);

  print("Server Available\n");

  while (true) {
    try {
      local socket = server.accept();
      print("connected\n");
      socket.binary(false);

      while (socket.isconnected()) {
        local v = getCrank();
        /*print("V: " + v + "\n")*/
        socket.writestr("" + v)
        delay(50);
      }
      print("disconnected\n")
    }
    catch (error)
    {
      print(error + "\n");
      delay(100);
    }
  }
}
