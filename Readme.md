# Zach's Electronic Hurdy Gurdy

See <http://www.zachcapalbo.com/projects/hurdy_gurdy.html>

This is the source code for the [Electronic Hurdy Gurdy][hg]. It runs on an [Esquilo](http://www.esquilo.io).

[hg]: http://www.zachcapalbo.com/projects/hurdy_gurdy.html
