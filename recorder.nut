isRecording <- false;
function startRecording()
{
  require("WebSocket");
  server <- WebSocketServer();

  print("waiting for connection\n");
  while (!server.available())
    delay(10);

  recordingSocket <- server.accept();
  print("connected\n");
  recordingSocket.binary(false);
  isRecording = true
}

function recordNote(note)
{
  if (isRecording && recordingSocket.isconnected())
  {
    recordingSocket.writestr(note)
  }
}
