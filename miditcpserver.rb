require 'midi'
require 'socket'

# system("sudo modprobe snd-virmidi snd_index=1")

o = UniMIDI::Output.first
server = TCPServer.new 2023

MIDI.using(o) do
  loop do
    Thread.start(server.accept) do |client|
      while !client.eof? do
        l = client.gets
        puts l
        begin
          eval(l)
        rescue Exception
          puts $!
        end
        client.puts
      end
    end
  end
end
