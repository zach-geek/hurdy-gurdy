require("GPIO");
require("HTTPC");
require("ADC");
require("math");

/*dofile("sd:/hurdygurdy/midi.nut")*/
dofile("sd:/hurdygurdy/synth.nut")
dofile("sd:/hurdygurdy/buttons.nut")
dofile("sd:/hurdygurdy/tunings.nut")
dofile("sd:/hurdygurdy/crank.nut")
dofile("sd:/hurdygurdy/recorder.nut")

function findNote () {
  local highest = buttons[0]
  foreach (b in buttons) {
    if (b.pressed()) {
      highest = b;
    }
  }

  local shiftedUp = shifter.pressed()
  local shiftedDown = downShifer.pressed()

  if (shiftedUp && shiftedDown)
  {
    return highest.special;
  } else if (shiftedUp) {
    return highest.shifted;
  } else if (shiftedDown) {
    return highest.lowered;
  } else {
    return highest.note;
  }
}

function findDrone() {
  local i = 0;
  local val = 0;
  foreach (d in drones) {
    if (d.pressed())
    {
      val = val | (1 << i);
    }
    i++;
  }

  if (val > 0 && val <= droneNotes.len())
  {
    return droneNotes[val - 1];
  }

  return null;

}

lastOctave <- " "
lastNote <- "G0"
lastDrone <- null

sink <- SynthSink();

while (true)
{
  sink.setPressure(getCrank());
  tuningKnob.checkTuning()

  local d = findDrone()

  if (d != lastDrone)
  {
    if (d == null)
    {
      sink.droneOff(lastDrone);
    } else {
      if (lastDrone != null) {
        sink.droneOff(lastDrone);
      }
      sink.droneOn(d)
    }
    lastDrone = d
  }

  local n = findNote()
  if (lastNote != n)
  {
    /*print("Note " + n + "\n")*/
    sink.noteOff(lastNote)
    sink.noteOn(n)
    recordNote(n)
    lastNote = n
  }
  sink.endLoop();
}
