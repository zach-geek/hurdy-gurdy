require("DAC");
require("math");
require("PWM")

dofile("sd:/hurdygurdy/pitches.nut")

class SynthSink {
  pressure = null
  freq = null
  sinewave = null
  pwm = null
  dronePwm = null
  droneFreq = 0

  constructor() {
    pressure = 0.0
    freq = 0.0
    pwm = PWM(1)
    pwm.on(0)
    dronePwm = PWM(2)
  }

  function setPressure(_p) {
    pressure = _p
    pwm.duty_cycle(0, pressure)
    dronePwm.duty_cycle(0, pressure)
  }

  function noteOn(n) {
    freq = Pitches[n]

    // Detune a bit so it doesn't produce odd interference with the drone frequency
    if (freq % droneFreq == 0) {
      freq = freq + freq * 0.01
    }
    pwm.frequency(freq)
  }

  function noteOff(n) {
  }

  function droneOn(n) {
    dronePwm.on(0)

    local f = Pitches[n]
    dronePwm.frequency(f)
    droneFreq = f
  }

  function droneOff(n) {
    dronePwm.off(0)
    droneFreq = 0
  }

  function endLoop() {
    delay(10)
  }
}
