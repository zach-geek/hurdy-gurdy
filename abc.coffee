generateABC = (notes) ->
  notes = notes.filter (n) -> "duration" of n

  medianDuration = notes.slice(0).sort((a, b) ->
    if a.duration > b.duration then 1 else if a.duration < b.duration then -1 else 0
    )[notes.length // 2].duration

  quantize = (note) ->
    ratio = 2.0 * note.duration / medianDuration
    return Math.round(ratio)

  represent = (note) ->
    note.rep = ''
    note.rep += '^' if note.sharp
    if note.octave <= 4
      note.rep += note.letter.toUpperCase()
      note.rep += ',' for i in [note.octave...4]
    if note.octave >= 5
      note.rep += note.letter.toLowerCase()
      note.rep += '\'' for i in [5...note.octave]

    prep = note.rep
    if note.q == 1
      note.rep += "/"
    else if note.q > 2
      note.rep += "#{Math.floor(note.q / 2.0)}"
      note.rep += "-#{prep}/" if note.q % 2 == 1

  pitchize = (note) ->
    note.letter = note.data[0]
    note.sharp = note.data.length is 3
    note.octave = + note.data[note.data.length - 1]

  abcize = (note) ->
    note.q = quantize(note)
    pitchize(note)
    represent(note)

  abcize(note) for note in notes

  headers =
    X: "1"
    M: "4/4"
    L: "1/4"
    K: "C"
    Q: Math.round(medianDuration / 4)

  text = []
  text.push("#{k}: #{v}") for k,v of headers
  text.push("#{(note.rep  for note in notes).join(" ")}")

  return {headers: headers, notes:notes, medianDuration: medianDuration, text: text.join("\n")}

window.generateABC = generateABC if window?

# v = generateABC [{"data":"D4","duration":1140.0199999999995,"timeStamp":6301.380000000001},{"data":"E4","duration":570.5699999999997,"timeStamp":7441.400000000001},{"data":"FS4","duration":1119.585,"timeStamp":8011.97},{"data":"E4","duration":509.8199999999997,"timeStamp":9131.555},{"data":"D4","duration":570.8500000000004,"timeStamp":9641.375},{"data":"E4","duration":1199.420000000002,"timeStamp":10212.225},{"data":"FS4","duration":520.2949999999983,"timeStamp":11411.645000000002},{"data":"A4","duration":470.1749999999993,"timeStamp":11973.185000000001},{"data":"B4","duration":1597.0200000000023,"timeStamp":12506.08},{"data":"B4","duration":259.7950000000019,"timeStamp":14241.985},{"data":"A4","duration":958.9599999999991,"timeStamp":14571.810000000001},{"data":"D5","duration":458.0750000000007,"timeStamp":15583.875},{"data":"A4","duration":441.9400000000023,"timeStamp":16041.95},{"data":"B4","duration":1119.6749999999993,"timeStamp":16542.135000000002},{"data":"A4","duration":474.36499999999796,"timeStamp":17726.645},{"data":"FS4","duration":500.44500000000335,"timeStamp":18261.495},{"data":"E4","duration":1494.9500000000044,"timeStamp":18816.015},{"data":"D4","duration":35209.975,"timeStamp":20310.965000000004},{"data":"D4","timeStamp":55609.26500000001}]
#v = generateABC [{"data":"FS5","duration":639.3550000000032,"timeStamp":53423.075000000004},{"data":"D5","duration":283.4100000000035,"timeStamp":54107.770000000004},{"data":"FS5","duration":474.5400000000009,"timeStamp":54437.55},{"data":"D5","duration":968.9799999999959,"timeStamp":54912.090000000004},{"data":"A4","duration":519.9400000000096,"timeStamp":55881.07},{"data":"B4","duration":431.24499999999534,"timeStamp":56401.01000000001},{"data":"D5","duration":411.8950000000041,"timeStamp":56880.065},{"data":"B4","duration":442.67499999999563,"timeStamp":57338.450000000004},{"data":"A4","duration":1400.925000000003,"timeStamp":57781.125},{"data":"FS5","duration":709.8050000000003,"timeStamp":59182.05},{"data":"D5","duration":251.01499999999942,"timeStamp":59940.575000000004},{"data":"FS5","duration":420.0599999999977,"timeStamp":60191.590000000004},{"data":"FS5","duration":448.15000000000146,"timeStamp":60693.135},{"data":"D5","duration":419.7849999999962,"timeStamp":61201.78500000001},{"data":"FS5","duration":464.5650000000023,"timeStamp":61687.22},{"data":"E5","duration":467.45500000000175,"timeStamp":62203.795000000006},{"data":"A4","duration":449.52999999999884,"timeStamp":62671.25000000001},{"data":"B4","duration":511.1100000000006,"timeStamp":63120.780000000006},{"data":"CS5","duration":252.11999999999534,"timeStamp":63631.89000000001},{"data":"B4","duration":277.1450000000041,"timeStamp":63884.01},{"data":"A4","duration":709.6700000000055,"timeStamp":64161.155000000006},{"data":"FS5","duration":762.25,"timeStamp":64870.82500000001},{"data":"D5","duration":262.6350000000093,"timeStamp":65688.675},{"data":"FS5","duration":515.054999999993,"timeStamp":65951.31000000001},{"data":"D5","duration":811.6100000000006,"timeStamp":66519.80500000001},{"data":"A4","duration":540.0850000000064,"timeStamp":67331.41500000001},{"data":"B4","duration":479.94499999999243,"timeStamp":67871.50000000001},{"data":"D5","duration":449.88000000000466,"timeStamp":68351.445},{"data":"B4","duration":493.69499999999243,"timeStamp":68801.32500000001},{"data":"A4","duration":1106.6399999999994,"timeStamp":69295.02},{"data":"FS5","duration":689.1600000000035,"timeStamp":70401.66},{"data":"FS5","duration":441.77500000000873,"timeStamp":71411.81},{"data":"E5","duration":466.96500000001106,"timeStamp":71853.585},{"data":"A4","duration":441.05499999997846,"timeStamp":72320.55000000002},{"data":"B4","duration":238.28500000001804,"timeStamp":72761.605},{"data":"CS5","duration":260.6649999999936,"timeStamp":72999.89000000001},{"data":"D5","duration":879.7200000000012,"timeStamp":73260.55500000001},{"data":"FS5","duration":466.4549999999872,"timeStamp":74204.67000000001},{"data":"D5","duration":953.0600000000122,"timeStamp":74739.26},{"data":"A4","timeStamp":75692.32}]
#console.log(v.text)
